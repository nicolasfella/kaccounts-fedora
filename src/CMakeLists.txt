#
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2020 Nicolas Fella <nicolas.fella@gmx.de>
#

add_library(kaccounts_plugin_fedora MODULE
    fedora.cpp
    fedoracontroller.cpp
)

target_link_libraries(kaccounts_plugin_fedora
                       Qt5::Core
                       KF5::I18n
                       KF5::Declarative
                       KAccounts
)

install(TARGETS kaccounts_plugin_fedora
         DESTINATION ${PLUGIN_INSTALL_DIR}/kaccounts/ui
)
kpackage_install_package(package org.kde.kaccounts.fedora genericqml)
