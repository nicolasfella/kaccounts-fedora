/*
 *  SPDX-FileCopyrightText: 2020 Nicolas Fella <nicolas.fella@gmx.de>
 *  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "fedora.h"
#include "fedoracontroller.h"

#include <KDeclarative/QmlObject>
#include <KLocalizedString>

#include <QIcon>
#include <QQmlContext>
#include <QQmlEngine>
#include <QWindow>

Fedora::Fedora(QObject *parent)
    : KAccountsUiPlugin(parent)
{
}

Fedora::~Fedora()
{
}

void Fedora::init(KAccountsUiPlugin::UiType type)
{
    if (type == KAccountsUiPlugin::NewAccountDialog) {
        const QString packagePath(QStringLiteral("org.kde.kaccounts.fedora"));

        m_object = new KDeclarative::QmlObject();
        m_object->setTranslationDomain(packagePath);
        m_object->setInitializationDelayed(true);
        m_object->loadPackage(packagePath);

        FedoraController *helper = new FedoraController(m_object);

        connect(helper, &FedoraController::wizardFinished, this, [this](const QString &username, const QString &password, const QVariantMap &data) {
            m_object->deleteLater();
            Q_EMIT success(username, password, data);
        });

        connect(helper, &FedoraController::wizardCancelled, this, [this] {
            m_object->deleteLater();
            Q_EMIT canceled();
        });

        m_object->engine()->rootContext()->setContextProperty(QStringLiteral("helper"), helper);

        m_object->completeInitialization();

        if (!m_object->package().metadata().isValid()) {
            return;
        }

        Q_EMIT uiReady();
    }
}

void Fedora::setProviderName(const QString &providerName)
{
    Q_UNUSED(providerName)
}

void Fedora::showNewAccountDialog()
{
    QWindow *window = qobject_cast<QWindow *>(m_object->rootObject());
    if (window) {
        window->setTransientParent(transientParent());
        window->show();
        window->requestActivate();
        window->setTitle(m_object->package().metadata().name());
        window->setIcon(QIcon::fromTheme(m_object->package().metadata().iconName()));
    }
}

void Fedora::showConfigureAccountDialog(const quint32 accountId)
{
    Q_UNUSED(accountId)
}

QStringList Fedora::supportedServicesForConfig() const
{
    return {};
}
