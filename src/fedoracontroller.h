/*
 *  SPDX-FileCopyrightText: 2020 Nicolas Fella <nicolas.fella@gmx.de>
 *  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */
#pragma once

#include <QObject>
#include <QStringList>
#include <QVariant>

class FedoraController : public QObject
{
    Q_OBJECT
public:
    explicit FedoraController(QObject *parent = nullptr);

    Q_INVOKABLE void cancel();
    Q_INVOKABLE void login(const QString &username, const QString &password);

Q_SIGNALS:
    void wizardFinished(const QString &username, const QString &password, const QVariantMap &data);
    void wizardCancelled();
};
