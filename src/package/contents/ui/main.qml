/*
 *  SPDX-FileCopyrightText: 2020 Nicolas Fella <nicolas.fella@gmx.de>
 *  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

import QtQuick 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.10

import org.kde.kirigami 2.13 as Kirigami

Kirigami.ApplicationWindow {

    width: Kirigami.Units.gridUnit * 20
    height: Kirigami.Units.gridUnit * 12

    onClosing: {
        helper.cancel()
    }

    pageStack.initialPage: Kirigami.Page {

        title: i18n("Login to Fedora")

        Kirigami.FormLayout {
            anchors.fill: parent

            TextField {
                id: userName
                placeholderText: i18n("johndoe")
                Kirigami.FormData.label: i18n("Username:")
            }
            Kirigami.PasswordField {
                id: password
                Kirigami.FormData.label: i18n("Password:")
            }
        }

        footer: ToolBar {
            RowLayout {
                anchors.fill: parent

                Button {
                    text: i18n("Ok")
                    Layout.alignment: Qt.AlignRight
                    enabled: userName.text.length > 0 && password.text.length > 0
                    onClicked: {
                        helper.login(userName.text, password.text)
                    }
                }
            }
        }
    }
}
