/*
 *  SPDX-FileCopyrightText: 2020 Nicolas Fella <nicolas.fella@gmx.de>
 *  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "fedoracontroller.h"

#include <QDebug>
#include <QProcess>

#include <KLocalizedString>

FedoraController::FedoraController(QObject *parent)
    : QObject(parent)
{
}

void FedoraController::cancel()
{
    Q_EMIT wizardCancelled();
}

void FedoraController::login(const QString &username, const QString &password)
{
    const QString userPrinciple = username + QLatin1String("@FEDORAPROJECT.ORG");

    // HACK port this to proper gssapi calls
    QProcess p;
    p.setProgram(QStringLiteral("kinit"));
    p.setArguments({userPrinciple});
    p.start();
    p.write(password.toLatin1());
    p.write("\n");

    p.waitForFinished();
    qDebug() << "login done";

    Q_EMIT wizardFinished(username, password, {});
}
